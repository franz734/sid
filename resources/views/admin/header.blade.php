<head>
    
    <!-- Title -->
    <title>Example</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{URL::asset('assets/plugins/materialize/css/materialize.min.css')}}"/>    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
    <link href="{{URL::asset('assets/plugins/metrojs/MetroJs.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/weather-icons-master/css/weather-icons.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/material-preloader/css/materialPreloader.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/chart.js/Chart.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/select2/css/select2.css')}}" rel="stylesheet" />    
    <link href="{{URL::asset('assets/plugins/materialalert/materialert.css')}}" rel="stylesheet" />    
    <!-- Theme Styles -->
    <link href="{{URL::asset('assets/css/alpha.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/css/custom.css')}}" rel="stylesheet" />  
    <!-- -->
    @yield('css')      
    
</head>