{{-- <div class="page-footer">        
    <div class="miPleca"></div>
</div> --}}

<!-- Javascripts -->
<script src="{{URL::asset('assets/plugins/jquery/jquery-2.2.0.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/materialize/js/materialize.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/material-preloader/js/materialPreloader.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-blockui/jquery.blockui.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
{{-- <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> --}}
<script src="{{URL::asset('assets/plugins/sweetalert2/sweetalert2.all.js')}}"></script>
<script src="{{URL::asset('assets/plugins/parsleyjs/parsley.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/parsleyjs/i18n/es.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-validation/localization/messages_es.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-steps/jquery.steps.js')}}"></script>
<script src="{{URL::asset('assets/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="{{URL::asset('assets/plugins/materialalert/materialert.js')}}"></script>
{{-- <script src="{{URL::asset('assets/plugins/dropify/js/dropify.min.js')}}}"></script> --}}
<script src="{{URL::asset('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>
<script src="{{URL::asset('assets/js/alpha.min.js')}}"></script>
<script src="{{URL::asset('assets/js/custom.js')}}?{{rand()}}"></script>
<!-- -->
@yield('scripts')