@include('admin.header')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Example</title>
</head>

<body class="signin-page">
    <div class="mn-content valign-wrapper">
        <main class="mn-inner container">
            <div class="valign">
                <div class="row">
                    {{-- <div style="text-align: center">
                        <img src="{{ URL::asset('/assets/images/logo_profepa.png') }}" width="300px">
                    </div> --}}
                    <div class="col s12 m6 l4 offset-l4 offset-m3">
                        <div class="card white darken-1" style="background-color: rgba(172, 162, 153, .7) !important">
                            <div class="card-content ">
                                <div class="row">
                                    <div style="text-align: center">
                                        <img src="{{ URL::asset('/assets/images/logo_profepa.png') }}" width="300px">
                                    </div>
                                    <form class="col s12" method="POST" {{-- action="{{ route('login') }}" --}}>
                                        @csrf
                                        <div class="input-field col s12">
                                            <input id="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror" name="email"
                                                value="{{ old('email') }}" required autocomplete="email">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <label for="email" style="color: black !important">Correo</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="password" type="password"
                                                class="form-control @error('password') is-invalid @enderror"
                                                name="password" required autocomplete="current-password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <label for="password" style="color: black !important">Contraseña</label>
                                        </div>
                                        <div class="col s12 right-align m-t-sm">
                                            <a href="{{ url('/inicio') }}"
                                                class="waves-effect waves-blue btn">Entrar</a>
                                            {{-- <button type="submit" class="btn btn-primary">
                                                    {{ __('Login') }}
                                                </button> --}}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    @include('admin.footer')
    <script>
        var url = document.URL;
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function() {
            history.pushState(null, null, url);
        });

    </script>
</body>

</html>
