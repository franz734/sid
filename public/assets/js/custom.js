// Funciones //
function clear_form_elements(class_name) {
  jQuery(class_name).find(':input').each(function () {
    switch (this.type) {
      case 'password':
      case 'text':
      case 'textarea':
      case 'file':
      case 'select-one':
      case 'select-multiple':
      case 'date':
      case 'number':
      case 'tel':
      case 'email':
        jQuery(this).val('');
        break;
      case 'checkbox':
      case 'radio':
        this.checked = false;
        break;
    }
  });
}

function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57)
}

function format(d) {
  return '<h5>Denunciante</h5>' +
    '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
    '<tr>' +
    '<th>Nombre / Razón Social</th>' +
    '<th>Entidad</th>' +
    '<th>Municipio</th>' +
    '<th>Dirección</th>' +
    '</tr>' +
    '<tr>' +
    '<td>' + d.datos_parte_rel[0].nomParte_razonSocial + '</td>' +
    '<td>' + d.datos_parte_rel[0].entidad_rel.nomEntidad + '</td>' +
    '<td>el municipio</td>' +
    '<td>' + d.datos_parte_rel[0].direccion + '</td>' +
    '</tr>' +
    '</table>' +
    '<h5>Denunciado</h5>' +
    '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
    '<tr>' +
    '<th>Nombre / Razón Social</th>' +
    '<th>Entidad</th>' +
    '<th>Municipio</th>' +
    '<th>Dirección</th>' +
    '</tr>' +
    '<tr>' +
    '<td>' + d.datos_parte_rel[1].nomParte_razonSocial + '</td>' +
    '<td>' + d.datos_parte_rel[1].entidad_rel.nomEntidad + '</td>' +
    '<td>el municipio</td>' +
    '<td>' + d.datos_parte_rel[1].direccion + '</td>' +
    '</tr>' +
    '</table>';
}

var spanish = {
  "sProcessing": "Procesando...",
  "sLengthMenu": "Mostrar _MENU_ registros",
  "sZeroRecords": "No se encontraron resultados",
  "sEmptyTable": "Ningún dato disponible en esta tabla =(",
  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix": "",
  "sSearch": "Buscar:",
  "sUrl": "",
  "sInfoThousands": ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
    "sFirst": '<i class="material-icons miTi">chevron_left</i>',
    "sLast": '<i class="material-icons miTi">chevron_right</i>',
    "sNext": '<i class="material-icons miTi">chevron_right</i>',
    "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
  },
  "oAria": {
    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  },
  "buttons": {
    "copy": "Copiar",
    "colvis": "Visibilidad"
  }
}
/////////////////////////////////

// Ready //
$(document).ready(function () {

  var datosParte = [];

  /// select2 ///
  $('.sel2').select2();
  /* $('#selectFuente').select2();
  $('#selectAtendio').select2();
  $('#selectArea').select2();
  $('#selectSubArea').select2();
  $('#selectProcedencia1').select2();
  $('#selectProcedencia2').select2();
  $('#selectProcedencia3').select2();
  $('#selectProcedencia4').select2();
  $('#selectTipoPer_1').select2();
  $('#selectSubTipo_1').select2();
  $('#selectGenero').select2();
  $('#selectEdo').select2();
  $('#selectMunicipio').select2();
  $('#selectTipoPer_2').select2(); */

  $('#anonimoI').val("0");
  $('#notifI').val("0");
  $('#tipoOrdenI').val("0");

  /// Mask ///
  $(".masked").inputmask("hh:mm", {
    mask: "h:s t\m",
    /* placeholder: "HH:MM", */
    insertMode: false,
    showMaskOnHover: false,
    hourFormat: 12
  });

  // Switch //
  $('#anonimo').change(function () {
    if ($(this).prop('checked')) {
      $('#anonimoI').val("1");
    }
    else {
      $('#anonimoI').val("0");
    }
  });
  $('#notif').change(function () {    
    if ($(this).prop('checked')) {
      $('#notifI').val("1");
    }
    else {
      $('#notifI').val("0");
    }
  });
  $('#tipoOrden').change(function () {    
    if ($(this).prop('checked')) {
      $('#tipoOrdenI').val("1");
      $('#justifExt').prop( "disabled", false );
    }
    else {
      $('#tipoOrdenI').val("0");
      $('#justifExt').val('');
      $('#justifExt').prop( "disabled", true );
    }
  });

  ///** Denuncia **///
  //select anidado areas //
  $('#selectArea').change(function () {
    $('#selectSubArea').val('def').trigger('change');
    var subAreaArr = [];
    var area = $(this).val();
    $.ajax({
      url: "listaSubAreas/" + area,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        for (const i in result) {
          if (result[i] != null) {
            subAreaArr.push({
              id: result[i].idSubArea,
              subArea: result[i].nomSubArea,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Sub-Área</option>';
        for (const i in subAreaArr) {
          cadCom += '<option value="' + subAreaArr[i].id + '">' + subAreaArr[i].subArea
            + '</option>';
        }
        $('#selectSubArea').empty()
        $('#selectSubArea').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });
  ///////////////////////////////////////

  ///**  Denunciante **///
  /// select tipo persona denunciante ///
  $('#selectTipoPer_1').change(function () {
    tipoPer_1 = $(this).val();
    console.log(tipoPer_1);
    $('#selectSubTipo_1').val('def').trigger('change');
    var subTipoArr = [];    
    $.ajax({
      url: "listaSubTipos/" + tipoPer_1 + "/" + 1,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {        
        for (const i in result) {
          if (result[i] != null) {
            subTipoArr.push({
              id: result[i].idSubTipo,
              subTipo: result[i].subTipo,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Sub-Tipo</option>';
        for (const i in subTipoArr) {
          cadCom += '<option value="' + subTipoArr[i].id + '">' + subTipoArr[i].subTipo
            + '</option>';
        }
        $('#selectSubTipo_1').empty()
        $('#selectSubTipo_1').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });

    if (tipoPer_1 == 1) {
      $('#contDenunciante').hide();      
      $('#contName').empty()
      clear_form_elements('#contDenunciante');
       var cad = '<div class="input-field">'+
                  '<label for="nombre">Nombre</label>'+
                  '<input id="nombre" class="validar" name="nombre" type="text" required>'+
                  '</div>'+
                  '<div class="input-field">'+
                  '<label for="aPaterno">Apellido Paterno</label>'+
                  '<input id="aPaterno" class="validar" name="aPaterno" type="text" required>'+
                  '</div>'+
                  '<div class="input-field">'+
                  '<label for="aMaterno">Apellido Materno</label>'+
                  '<input id="aMaterno" class="validar" name="aMaterno" type="text" required>'+
                  '</div>';
      $('#contName').html(cad);
      $('#contDenunciante').show();      
    }
    else if (tipoPer_1 == 2) {
      $('#contDenunciante').hide();
      $('#contName').empty()            
      clear_form_elements('#contDenunciante');
      var cad = '<div class="input-field">'+
                '<label for="razonSocial">Razón Social</label>'+
                '<input id="razonSocial" class="validar" name="razonSocial" type="text" required>'+
                '</div>';
      $('#contName').html(cad);
      $('#contDenunciante').show();     
    }
    else if (tipoPer_1 == 3) {
      $('#contDenunciante').hide();
      $('#contName').empty()            
      clear_form_elements('#contDenunciante');
      var cad = '<div class="input-field">'+
                '<label for="nomAutoridad">Autoridad</label>'+
                '<input id="nomAutoridad" class="validar" name="nomAutoridad" type="text" required>'+
                '</div>';
      $('#contName').html(cad);
      $('#contDenunciante').show();      
    }
  });
  //select anidado municipios //
  $('#selectEdo').change(function () {
    $('#selectMunicipio').val('def').trigger('change');
    var municipioArr = [];
    var entidad = $(this).val();
    $.ajax({
      url: "listaMunicipios/" + entidad,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        for (const i in result) {
          if (result[i] != null) {
            municipioArr.push({
              id: result[i].idMunicipio,
              municipio: result[i].nomMunicipio,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Delegación o Municipio</option>';
        for (const i in municipioArr) {
          cadCom += '<option value="' + municipioArr[i].id + '">' + municipioArr[i].municipio
            + '</option>';
        }
        $('#selectMunicipio').empty()
        $('#selectMunicipio').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });
  ///////////////////////////////////////

  ///**  Denunciado **///
  /// select tipo persona denunciante ///
  $('#selectTipoPer_2').change(function () {
    tipoPer_2 = $(this).val();
    console.log(tipoPer_2);
    $('#selectSubTipo_1').val('def').trigger('change');
    var subTipoArr = [];    
    $.ajax({
      url: "listaSubTipos/" + tipoPer_2 + "/" + 2,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {        
        for (const i in result) {
          if (result[i] != null) {
            subTipoArr.push({
              id: result[i].idSubTipo,
              subTipo: result[i].subTipo,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Sub-Tipo</option>';
        for (const i in subTipoArr) {
          cadCom += '<option value="' + subTipoArr[i].id + '">' + subTipoArr[i].subTipo
            + '</option>';
        }
        $('#selectSubTipo_2').empty()
        $('#selectSubTipo_2').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });

    if (tipoPer_2 == 1) {
      $('#contDenunciado').hide();      
      $('#contNameDdo').empty()
      clear_form_elements('#contDenunciado');
       var cad = '<div class="input-field">'+
                  '<label for="nombreDdo">Nombre</label>'+
                  '<input id="nombreDdo" class="validar" name="nombreDdo" type="text" required>'+
                  '</div>'+
                  '<div class="input-field">'+
                  '<label for="aPaternoDdo">Apellido Paterno</label>'+
                  '<input id="aPaternoDdo" class="validar" name="aPaternoDdo" type="text" required>'+
                  '</div>'+
                  '<div class="input-field">'+
                  '<label for="aMaternoDdo">Apellido Materno</label>'+
                  '<input id="aMaternoDdo" class="validar" name="aMaternoDdo" type="text" required>'+
                  '</div>';
      $('#contNameDdo').html(cad);
      $('#contDenunciado').show();      
    }
    else if (tipoPer_2 == 2) {
      $('#contDenunciado').hide();
      $('#contNameDdo').empty()            
      clear_form_elements('#contDenunciado');
      var cad = '<div class="input-field">'+
                '<label for="razonSocialDdo">Razón Social</label>'+
                '<input id="razonSocialDdo" class="validar" name="razonSocialDdo" type="text" required>'+
                '</div>';
      $('#contNameDdo').html(cad);
      $('#contDenunciado').show();      
    }
    else if (tipoPer_2 == 3) {
      $('#contDenunciado').hide();
      $('#contNameDdo').empty()            
      clear_form_elements('#contDenunciado');
      var cad = '<div class="input-field">'+
                '<label for="nomAutoridadDdo">Autoridad</label>'+
                '<input id="nomAutoridadDdo" class="validar" name="nomAutoridadDdo" type="text" required>'+
                '</div>';
      $('#contNameDdo').html(cad);
      $('#contDenunciado').show();      
    }
  });
  //select anidado municipios //
  $('#selectEdoDdo').change(function () {
    $('#selectMunicipioDdo').val('def').trigger('change');
    var municipioArr = [];
    var entidad = $(this).val();
    $.ajax({
      url: "listaMunicipios/" + entidad,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        for (const i in result) {
          if (result[i] != null) {
            municipioArr.push({
              id: result[i].idMunicipio,
              municipio: result[i].nomMunicipio,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Delegación o Municipio</option>';
        for (const i in municipioArr) {
          cadCom += '<option value="' + municipioArr[i].id + '">' + municipioArr[i].municipio
            + '</option>';
        }
        $('#selectMunicipioDdo').empty()
        $('#selectMunicipioDdo').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });
  ///////////////////////////////////////

  ///**  Acciones **///
  //select anidado municipios //
  $('#selectEdoAcc').change(function () {
    $('#selectMunicipioAcc').val('def').trigger('change');
    var municipioArr = [];
    var entidad = $(this).val();
    $.ajax({
      url: "listaMunicipios/" + entidad,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        for (const i in result) {
          if (result[i] != null) {
            municipioArr.push({
              id: result[i].idMunicipio,
              municipio: result[i].nomMunicipio,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Delegación o Municipio</option>';
        for (const i in municipioArr) {
          cadCom += '<option value="' + municipioArr[i].id + '">' + municipioArr[i].municipio
            + '</option>';
        }
        $('#selectMunicipioAcc').empty()
        $('#selectMunicipioAcc').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });

  ///datatable denuncias///
  var tableDenuncias = $('#tableDenuncias').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getDenuncias",
    "columns": [
      { "data": "" },
      { "data": "idDenuncia" },
      { "data": "expediente" },
      { "data": "fuente_rel.nomFuente" },
      { "data": "atiende" },
      { "data": "fcCrea" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": "estatus_rel",
        "render": function (dta, type, row) {
          if (dta.idEstatus == 1) {
            cad = '<p style="color:red">' + dta.nomEstatus + '</p>';
          }
          else if (dta.idEstatus == 2) {
            cad = '<a style="color:blue; background-color:#bfc9ca" class="waves-effect waves-light btn btnOrden">' + dta.nomEstatus + '</a>';
          }
          else if (dta.idEstatus == 3) {
            cad = '<a style="color:green; background-color:#bfc9ca" class="waves-effect waves-light btn btnActa">' + dta.nomEstatus + '</a>';
          }
          return cad;
        }
      },

    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableDenuncias tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tableDenuncias.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  //Botones dentro de una fila//
  //Orden//
  $('#tableDenuncias tbody').on('click', '.btnOrden', function () {
    var tr = $(this).closest('tr');
    var row = tableDenuncias.row(tr).data();
    console.log(row);
    cad = '<div class="modal-content">' +
      '<p><b>id Orden</b>: ' + row.orden_rel.idOrden + '</p>' +
      '<p><b>fecha Orden</b>: ' + row.orden_rel.fcCrea + '</p>' +
      '</div>' +
      '<div class="modal-footer">' +
      '</div>';
    $('#modalInfo').html(cad)
    $('#modalInfo').openModal();

  });
  //Acta//
  $('#tableDenuncias tbody').on('click', '.btnActa', function () {
    var tr = $(this).closest('tr');
    var row = tableDenuncias.row(tr).data();
    console.log(row);
    cad = '<div class="modal-content">' +
      '<p><b>id Acta</b>: ' + row.acta_rel.idActa + '</p>' +
      '<p><b>fecha Orden</b>: ' + row.acta_rel.fcCrea + '</p>' +
      '</div>' +
      '<div class="modal-footer">' +
      '</div>';
    $('#modalInfo').html(cad)
    $('#modalInfo').openModal();

  });

}); //ready
/////////////////////////////
/* $(document).on('click', '.eliminaColum', function(e){
    e.preventDefault();
    $(this).parent('li').remove();
    var id = $(this).data('id');
    delete columnas[id];
    console.log(columnas);
  }); */
